# pruebacidenet

Para la instalación de los proyectos frontend como backend al momento de abrirlos con el IDE Visual Studio Code, por medio
de la terminal ejecutar el comando npm install.

Para la ejecución del proyecto frontend llamado prueba ejecutar el comando npm start.

Desde XAMPP ejecutar en la consola de MySql el script llamado tutorials.sql el cual se encargará de crear la base de datos y las tablas que se utilizarán en el proyecto backend

Para la ejecución del proyecto backend llamado nodejsapi ejecutar desde el menú principal del IDE Visual Studio Code la pestaña Run y luego dar click en Start Debugging.