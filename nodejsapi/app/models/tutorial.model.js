module.exports = (sequelize, Sequelize) => {
  const Tutorial = sequelize.define("tutorial", {
    title: {
      type: Sequelize.STRING,
    },
    description: {
      type: Sequelize.STRING,
    },
    department: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    typeidentification: {
      type: Sequelize.STRING,
    },
    country: {
      type: Sequelize.STRING,
    },
    numberidentification: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    dateentrance: {
      type: Sequelize.STRING,
    },
    area: {
      type: Sequelize.STRING,
    },
  });
  return Tutorial;
};
