// Se hacen las importaciones generales del proyecto

import React, { useState } from "react";
import "./App.css";
import { Button, Card, Modal, FormGroup, FormLabel } from "react-bootstrap";
import * as Yup from "yup";
import { ErrorMessage, Field, Formik, Form } from "formik";
import moment from "moment";

export const App = () => {
  // Se definen las variables para las vistas

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // Esquema para las validaciones de los campos

  const formSchema = Yup.object().shape({
    FirstLastName: Yup.string()
      .max(20, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
    SecondLastName: Yup.string()
      .max(20, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
    FirstName: Yup.string()
      .max(20, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
    OtherName: Yup.string().max(50, `Máximo 50 caracteres`),
    NumberId: Yup.string()
      .max(50, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
  });

  // Función para guardar los datos almacenados en cada uno de los campos

  const Save = () => {
    fetch("http://localhost:8080/api/tutorials", {
      method: "POST",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        city: datos.OtherName,
        department: datos.FirstName,
        description: datos.SecondLastName,
        title: datos.FirstLastName,
        typeidentification: datos.TypeId,
        country: datos.Country,
        numberidentification: datos.NumberId,
        email: `${datos.FirstName}.${datos.FirstLastName}@cidenet.com.${
          datos.Country === "Colombia" ? "co" : "us"
        }`,
        dateentrance: datos.DateEntrance,
        area: datos.Area,
      }),
    }).catch((err) => console.log(err));
  };

  // Se inicializan todas las variables que son utilizadas para cada uno de los campos

  const [datos, setDatos] = useState({
    FirstLastName: "",
    SecondLastName: "",
    FirstName: "",
    OtherName: "",
    TypeId: "",
    Country: "",
    NumberId: "",
    DateEntrance: "",
    Area: "",
  });

  // Evento que escucha los cambios efectuados en cada uno de los campos

  const handleInputChange = (event) => {
    setDatos({
      ...datos,
      [event.target.name]: event.target.value.toUpperCase(),
    });
  };

  // Renderización de toda la vista

  return (
    <div className="App">
      <Button variant="primary" onClick={handleShow}>
        Registrar Empleado
      </Button>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Registrar Empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Card
            style={{
              width: "28rem",
              padding: "44px",
              margin: "0 auto",
            }}
          >
            <Formik
              initialValues={{
                FirstLastName: "",
                SecondLastName: "",
                FirstName: "",
                OtherName: "",
                TypeId: "",
                Country: "",
                NumberId: "",
                DateEntrance: "",
                Area: "",
              }}
              validationSchema={formSchema}
              onSubmit={(values) => console.log(values)}
            >
              <Form onSubmit={Save}>
                <FormGroup>
                  <FormLabel>Primer Apellido:</FormLabel>
                  <Field
                    className="form-control"
                    name="FirstLastName"
                    placeholder="Ingrese el Apellido"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                    required
                  />
                  <ErrorMessage
                    name="FirstLastName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Segundo Apellido:</FormLabel>
                  <Field
                    className="form-control"
                    name="SecondLastName"
                    placeholder="Escriba su Segundo Apellido"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                    required
                  />
                  <ErrorMessage
                    name="SecondLastName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Primer Nombre:</FormLabel>
                  <Field
                    className="form-control"
                    name="FirstName"
                    placeholder="Escriba su Primer Nombre"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                    required
                  />
                  <ErrorMessage
                    name="FirstName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Otros Nombres:</FormLabel>
                  <Field
                    className="form-control"
                    name="OtherName"
                    placeholder="Escriba sus Otros Nombres"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                  />
                  <ErrorMessage
                    name="OtherName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>
                <FormGroup>
                  <FormLabel>País del empleo:</FormLabel>
                  <select
                    name="Country"
                    className="form-control"
                    onChange={(event) => handleInputChange(event)}
                  >
                    <option>Elija una opción</option>
                    <option value="Colombia">Colombia</option>
                    <option value="Estados Unidos">Estados Unidos</option>
                  </select>
                </FormGroup>
                <FormGroup>
                  <FormLabel>Tipo de Identificación:</FormLabel>
                  <select
                    name="TypeId"
                    className="form-control"
                    onChange={(event) => handleInputChange(event)}
                  >
                    <option>Elija una opción</option>
                    <option value="Cédula de Ciudadanía">
                      Cédula de Ciudadanía
                    </option>
                    <option value="Cédula de Extranjería">
                      Cédula de Extranjería
                    </option>
                    <option value="Pasaporte">Pasaporte</option>
                    <option value="Permiso Especial">Permiso Especial</option>
                  </select>
                  <ErrorMessage
                    name="TypeId"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>
                <FormGroup>
                  <FormLabel>Número de Identificación:</FormLabel>

                  <Field
                    className="form-control"
                    name="NumberId"
                    placeholder="Escriba su Número de Identificación"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                  />
                  <ErrorMessage
                    name="NumberId"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>
                <FormGroup>
                  <FormLabel>Fecha de Ingreso:</FormLabel>
                  <input
                    className="form-control"
                    name="DateEntrance"
                    type="date"
                    onChange={(event) => handleInputChange(event)}
                    max={moment(new Date()).format("YYYY-MM-DD")}
                    min={moment().subtract(1, "months").format("YYYY-MM-DD")}
                  />
                  <ErrorMessage
                    name="DateEntrance"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Área</FormLabel>
                  <select
                    name="Area"
                    className="form-control"
                    onChange={(event) => handleInputChange(event)}
                  >
                    <option>Elija una opción</option>
                    <option value="Administración">Administración</option>
                    <option value="Financiera">Financiera</option>
                    <option value="Compras">Compras</option>
                    <option value="Infraestructura">Infraestructura</option>
                    <option value="Operación">Operación</option>
                    <option value="Talento Humano">Talento Humano</option>
                    <option value="Servicios Varios"> Servicios Varios</option>
                  </select>
                  <ErrorMessage
                    name="Area"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <Button
                  color="primary"
                  className="mr-1 mb-1 btn-block"
                  type="submit"
                >
                  Guardar
                </Button>
              </Form>
            </Formik>
          </Card>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
