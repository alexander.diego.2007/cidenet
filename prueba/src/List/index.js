// Se hacen las importaciones generales del proyecto

import { ErrorMessage, Field, Formik } from "formik";
import React, { useState, useEffect } from "react";
import {
  Table,
  Button,
  Modal,
  Form,
  Card,
  FormGroup,
  FormLabel,
} from "react-bootstrap";
import { App } from "../App";
import { Page } from "./style";
import * as Yup from "yup";
import moment from "moment";

// Se crea la función general del componente el cual se encarga de ejecutar la vista y la lógica de esta sección

export const List = () => {
  // Se definen las variables generales que se van a utilizar en esta vista

  const [dataCurrent, setDataCurrent] = useState([]);
  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);

  const [id, setId] = useState(0);
  const [del, setDel] = useState(null);

  const handleClose = () => setShow(false);
  const handleCloseDelete = () => setShow1(false);

  // Función que se encarga de cambiar el estado para la muestra de el modal

  const handleShow = (e) => {
    setShow(true);
    setId(e);
  };

  const handleShowDelete = (e) => {
    setShow1(true);
    setDel(e);
  };
  useEffect(() => {
    fetch(`http://localhost:8080/api/tutorials/`, {
      method: "GET",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept",
      },
    })
      .then((response) => response.json())
      .then((data) => setDataCurrent(data));
  }, []);

  // Se inicializan los parámetros que se encargan de comunicar los datos para ser almacenados

  const [datos, setDatos] = useState({
    FirstLastName: "",
    SecondLastName: "",
    FirstName: "",
    OtherName: "",
    TypeId: "",
    Country: "",
    NumberId: "",
    DateEntrance: "",
    Area: "",
  });

  const handleInputChange = (event) => {
    setDatos({
      ...datos,
      [event.target.name]: event.target.value.toUpperCase(),
    });
  };

  // Se hace la petición al api para enviar los datos

  const SendForm = () => {
    fetch(`http://localhost:8080/api/tutorials/${id}`, {
      method: "PUT",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        city: datos.OtherName,
        department: datos.FirstName,
        description: datos.SecondLastName,
        title: datos.FirstLastName,
        typeidentification: datos.TypeId,
        country: datos.Country,
        numberidentification: datos.NumberId,
        email: `${datos.FirstName}.${datos.FirstLastName}@cidenet.com.${
          datos.Country === "Colombia" ? "co" : "us"
        }`,
        dateentrance: datos.DateEntrance,
        area: datos.Area,
      }),
    }).catch((err) => console.log(err));
  };

  // Función para ejecutar el DELETE al api para eliminar un registro

  const modalDelete = () => {
    fetch(`http://localhost:8080/api/tutorials/${del}`, {
      method: "DELETE",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers":
          "Origin, X-Requested-With, Content-Type, Accept",
        "Content-Type": "application/json",
      },
    }).catch((err) => console.log(err));
  };

  // Esquema para las validaciones de los campos

  const formSchema = Yup.object().shape({
    FirstLastName: Yup.string()
      .max(20, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
    SecondLastName: Yup.string()
      .max(20, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
    FirstName: Yup.string()
      .max(20, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
    OtherName: Yup.string().max(50, `Máximo 50 caracteres`),
    NumberId: Yup.string()
      .max(50, `Máximo 20 caracteres`)
      .required("Campo Requerido"),
  });

  // Se renderiza toda la vista

  return (
    <Page>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Editar Empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Card
            style={{
              width: "28rem",
              padding: "44px",
              margin: "0 auto",
            }}
          >
            <Formik
              initialValues={{
                FirstLastName: "",
                SecondLastName: "",
                FirstName: "",
                OtherName: "",
                typeidentification: "",
                Country: "",
                NumberId: "",
                DateEntrance: "",
                Area: "",
              }}
              validationSchema={formSchema}
              onSubmit={(values) => console.log(values)}
            >
              <Form onSubmit={SendForm}>
                <FormGroup>
                  <FormLabel>Primer Apellido:</FormLabel>
                  <Field
                    className="form-control"
                    name="FirstLastName"
                    placeholder="Ingrese el Apellido"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                    required
                  />
                  <ErrorMessage
                    name="FirstLastName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Segundo Apellido:</FormLabel>
                  <Field
                    className="form-control"
                    name="SecondLastName"
                    placeholder="Escriba su Segundo Apellido"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                    required
                  />
                  <ErrorMessage
                    name="SecondLastName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Primer Nombre:</FormLabel>
                  <Field
                    className="form-control"
                    name="FirstName"
                    placeholder="Escriba su Primer Nombre"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                    required
                  />
                  <ErrorMessage
                    name="FirstName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Otros Nombres:</FormLabel>
                  <Field
                    className="form-control"
                    name="OtherName"
                    placeholder="Escriba sus Otros Nombres"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                  />
                  <ErrorMessage
                    name="OtherName"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>
                <FormGroup>
                  <FormLabel>País del empleo:</FormLabel>
                  <select
                    name="Country"
                    className="form-control"
                    onChange={(event) => handleInputChange(event)}
                  >
                    <option>Elija una opción</option>
                    <option value="Colombia">Colombia</option>
                    <option value="Estados Unidos">Estados Unidos</option>
                  </select>
                </FormGroup>
                <FormGroup>
                  <FormLabel>Tipo de Identificación:</FormLabel>
                  <select
                    name="TypeId"
                    className="form-control"
                    onChange={(event) => handleInputChange(event)}
                  >
                    <option>Elija una opción</option>
                    <option value="Cédula de Ciudadanía">
                      Cédula de Ciudadanía
                    </option>
                    <option value="Cédula de Extranjería">
                      Cédula de Extranjería
                    </option>
                    <option value="Pasaporte">Pasaporte</option>
                    <option value="Permiso Especial">Permiso Especial</option>
                  </select>
                  <ErrorMessage
                    name="TypeId"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>
                <FormGroup>
                  <FormLabel>Número de Identificación:</FormLabel>

                  <Field
                    className="form-control"
                    name="NumberId"
                    placeholder="Escriba su Número de Identificación"
                    type="text"
                    onKeyUp={(event) => handleInputChange(event)}
                  />
                  <ErrorMessage
                    name="NumberId"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>
                <FormGroup>
                  <FormLabel>Fecha de Ingreso:</FormLabel>
                  <input
                    className="form-control"
                    name="DateEntrance"
                    type="date"
                    onChange={(event) => handleInputChange(event)}
                    max={moment(new Date()).format("YYYY-MM-DD")}
                    min={moment().subtract(1, "months").format("YYYY-MM-DD")}
                  />
                  <ErrorMessage
                    name="DateEntrance"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <FormGroup>
                  <FormLabel>Área</FormLabel>
                  <select
                    name="Area"
                    className="form-control"
                    onChange={(event) => handleInputChange(event)}
                  >
                    <option>Elija una opción</option>
                    <option value="Administración">Administración</option>
                    <option value="Financiera">Financiera</option>
                    <option value="Compras">Compras</option>
                    <option value="Infraestructura">Infraestructura</option>
                    <option value="Operación">Operación</option>
                    <option value="Talento Humano">Talento Humano</option>
                    <option value="Servicios Varios"> Servicios Varios</option>
                  </select>
                  <ErrorMessage
                    name="Area"
                    component="div"
                    className="field-error text-danger"
                  />
                </FormGroup>

                <Button
                  color="primary"
                  className="mr-1 mb-1 btn-block"
                  type="submit"
                >
                  Guardar
                </Button>
              </Form>
            </Formik>
          </Card>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={show1} onHide={handleCloseDelete}>
        <Modal.Header closeButton>
          <Modal.Title>Eliminar Empleado</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Card
            style={{
              width: "28rem",
              padding: "44px",
              margin: "0 auto",
            }}
          >
            <Form onSubmit={modalDelete}>
              <Button
                variant="danger"
                className="mr-1 mb-1 btn-block"
                type="submit"
              >
                ¿Esta seguro de eliminar el registro?
              </Button>
            </Form>
          </Card>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDelete}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      <h1>Lista de Empleados</h1>
      <Table responsive>
        <thead>
          <tr>
            <th>Primer Apellido</th>
            <th>Segundo Apellido</th>
            <th>Primer Nombre</th>
            <th>Otros Nombres</th>
            <th>Tipo de Identificación</th>
            <th>País del empleo</th>
            <th>Número de Identificación</th>
            <th>Correo electrónico</th>
            <th>Fecha de Ingreso</th>
            <th>Área</th>
            <th>Estado</th>
            <th>Fecha y hora de registro</th>
          </tr>
        </thead>
        <tbody>
          {dataCurrent?.map((item, index) => {
            return (
              <tr className="update" key={index}>
                <td>{item.title}</td>
                <td>{item.description}</td> <td>{item.department}</td>
                <td>{item.city}</td> <td>{item.typeidentification}</td>
                <td>{item.country}</td> <td>{item.numberidentification}</td>
                <td style={{ textTransform: "lowercase" }}>{item.email}</td>
                <td>{moment(item.dateentrance).format("DD/MM/YYYY")}</td>
                <td>{item.area}</td>
                <td>Activo</td>
                <td>{moment(item.createdAt).format("DD/MM/YYYY HH:mm:ss")}</td>
                <td>
                  <Button variant="warning" onClick={() => handleShow(item.id)}>
                    Editar
                  </Button>
                </td>
                <td>
                  <Button
                    variant="danger"
                    onClick={() => handleShowDelete(item.id)}
                  >
                    Eliminar
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
      <App />
    </Page>
  );
};
