-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2020 a las 07:47:04
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: testdb
--
   CREATE DATABASE testdb
 
   USE testdb
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tutorials
--

CREATE TABLE tutorials (
  id int(255) NOT NULL,
  title varchar(255) DEFAULT NULL,
  description varchar(255) DEFAULT NULL,
  department varchar(255) DEFAULT NULL,
  city varchar(255) NOT NULL,
  typeidentification varchar(255) NOT NULL,
  country varchar(255) NOT NULL,
  numberidentification int(11) NOT NULL,
  email varchar(255) NOT NULL,
  dateentrance datetime NOT NULL DEFAULT current_timestamp(),
  area varchar(255) NOT NULL,
  createdAt datetime NOT NULL,
  updatedAt datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla tutorials
--

INSERT INTO tutorials (id, title, description, department, city, typeidentification, country, numberidentification, email, dateentrance, `area`, createdAt, updatedAt) VALUES
(98, 'ARDILA', 'ROZO', 'DIEGO', 'ALEXANDER', 'CÉDULA DE CIUDADANÍA', 'ESTADOS UNIDOS', 353536464, 'DIEGO.ARDILA.@cidenet.com.us', '2020-11-05 00:00:00', 'FINANCIERA', '2020-11-25 06:05:36', '2020-11-25 06:05:36');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla tutorials
--
ALTER TABLE tutorials
  ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla tutorials
--
ALTER TABLE tutorials
  MODIFY id int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
